const Gio = imports.gi.Gio;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const AppFavorites = imports.ui.appFavorites;
const AppDisplay = imports.ui.appDisplay;
const BoxPointer = imports.ui.boxpointer;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;

const FavsView = new Lang.Class({
    Name: 'FavsView',
    Extends: AppDisplay.BaseAppView,

    _init: function() {
        this.parent(null, { fillParent: true });

        let cancelAnimation = this._grid._cancelAnimation;
        this._grid._cancelAnimation = () => {
            try { throw new Error(); } catch(e) {
                let m = e.stack.split('\n')[1].match(/animate(Spring|Pulse)/);
                if (m != null) // Ignore calls from anonymous notify::mapped
                    cancelAnimation.call(this._grid);
            }
        };

        this.actor = new St.Widget({ x_expand: true, y_expand: true });
        this.actor.add_actor(this._grid.actor);

        this._favorites = AppFavorites.getAppFavorites();
        this._favorites.connect('changed', () => { this._redisplay(); });

        this._redisplay();
    },

    _loadApps: function() {
        this._favorites.getFavorites().forEach(fav => {
            let icon = new AppDisplay.AppIcon(fav, { isDraggable: false });
            this._grid.addItem(icon, -1);
        });
    },

    adaptToSize: function(width, height) {
        this._grid.adaptToSize(width, height);
    }
});

const AllView = new Lang.Class({
    Name: 'MyAllView',
    Extends: AppDisplay.FolderView,

    _init: function() {
        this.parent();
        this._appSys = Shell.AppSystem.get_default();

        this.setPaddingOffsets(0);
        this._redisplay();
    },

    _loadApps: function() {
        let ids = Gio.AppInfo.get_all().filter(info => {
            try {
                let id = info.get_id(); // catch invalid file encodings
            } catch(e) {
                return false;
            }
            return info.should_show();
        }).map(info => info.get_id());

        ids.forEach(id => {
            let app = this._appSys.lookup_app(id);
            let icon = new AppDisplay.AppIcon(app, { isDraggable: false });
            this.addItem(icon);
        });
        this.loadGrid();
    },

    adaptToSize: function(width, height) {
        this._grid.actor.ensure_style();

        let padding = Math.round((height - this.usedHeight()) / 2);
        this._grid.topPadding = padding;
        this._grid.bottomPadding = padding;

        this.parent(width, height);
        this.actor.height = height;
    }
});

const ShowAllButton = new Lang.Class({
    Name: 'ShowAllButton',
    Extends: St.Button,

    _init(view) {
        this.parent({ style_class: 'app-view-control button',
                      toggle_mode: true });

        this.set_child(new St.Icon({ icon_name: 'view-more-symbolic' }));

        this._menu = new PopupMenu.PopupMenu(this, 0.5, St.Side.BOTTOM);
        Main.uiGroup.add_actor(this._menu.actor);
        this._menu.actor.hide();

        this._menu.box.add_actor(view.actor);

        this.actor = this; // for GrabHelper
        this._menuManager = new PopupMenu.PopupMenuManager(this);
        this._menuManager.addMenu(this._menu);

        this._menu.connect('open-state-changed', (menu, isOpen) => {
            if (isOpen)
                view.animate(IconGrid.AnimationDirection.IN);
            else
                this.checked = false;
        });

        this.connect('notify::checked', () => {
            if (this.checked)
                this._menu.open(BoxPointer.PopupAnimation.FULL);
            else
                this._menu.close(BoxPointer.PopupAnimation.FULL);
        });

        this.connect('destroy', () => {
            this._menu.destroy();
        });
    }
});

let favsView, favsButton;
let allView, allButton;
let viewIndex;

function enable() {
    favsView = new FavsView();
    favsButton = new St.Button();

    let display = Main.overview.viewSelector.appDisplay;
    viewIndex = display._views.push({ 'view': favsView,
                                      'control': favsButton }) - 1;

    display._viewStack.add_actor(favsView.actor);
    display._controls.add_actor(favsButton);

    display._showView(viewIndex);

    allView = new AllView();
    allButton = new ShowAllButton(allView);

    display._views.push({ 'view': allView, 'control': allButton }) - 1;

    display._controls.add_actor(allButton);

    display._controls.get_children().forEach(c => { c.visible = c == allButton; });
}

function disable() {
    let display = Main.overview.viewSelector.appDisplay;

    display._showView(AppDisplay.Views.ALL);

    display._views.splice(viewIndex, 2);
    favsView.actor.destroy();
    favsButton.destroy();

    allView.actor.destroy();
    allButton.destroy();

    display._controls.get_children().forEach(c => { c.show(); });
}
