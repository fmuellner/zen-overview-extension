const Clutter = imports.gi.Clutter;
const Lang = imports.lang;
const Shell = imports.gi.Shell;
const St = imports.gi.St;

const ExtensionUtils = imports.misc.extensionUtils;
const IconGrid = imports.ui.iconGrid;
const Main = imports.ui.main;
const OverviewControls = imports.ui.overviewControls;
const Overview = imports.ui.overview;
const PanelMenu = imports.ui.panelMenu;
const Tweener = imports.ui.tweener;
const ViewSelector = imports.ui.viewSelector;
const WorkspaceThumbnail = imports.ui.workspaceThumbnail;

const Me = ExtensionUtils.getCurrentExtension();
const AppsView = Me.imports.appsView;

function reloadKeybindings() {
    let modes = Shell.ActionMode.NORMAL | Shell.ActionMode.OVERVIEW;

    let viewSelector = Main.overview.viewSelector;
    Main.wm.setCustomKeybindingHandler('toggle-application-view', modes,
                                       Lang.bind(viewSelector,
                                                 viewSelector._toggleAppsPage));
   Main.wm.setCustomKeybindingHandler('toggle-overview', modes,
                                      Lang.bind(Main.overview,
                                                Main.overview.toggle));
}

function setSearchVisible(visible) {
    let actor = Main.overview._searchEntryBin;

    let opacity = visible ? 255 : 0;
    let translation_y = visible ? 0 : -(actor.y + actor.height);
    let time = OverviewControls.SIDE_CONTROLS_ANIMATION_TIME;
    let transition = 'easeOutQuad';
    Tweener.addTween(actor, { opacity, translation_y, time, transition });
}

const AppsButton = new Lang.Class({
    Name: 'AppsButton',
    Extends: PanelMenu.Button,

    _init: function() {
        this.parent(0.0, null, true);
        //this.actor.accessible_role = Atk.Role.TOGGLE_BUTTON;

        let icon = new St.Icon({ icon_name: 'view-app-grid-symbolic',
                                 style_class: 'system-status-icon' });
        this.actor.add_actor(icon);
    },

    _onEvent: function(actor, event) {
        this.parent(actor, event);

        if (event.type() == Clutter.EventType.TOUCH_END ||
            event.type() == Clutter.EventType.BUTTON_RELEASE)
            Main.overview.viewSelector._toggleAppsPage();

        return Clutter.EVENT_PROPAGATE;
    }
});

const ThumbBorderConstraint = new Lang.Class({
    Name: 'ThumbBorderConstraint',
    Extends: Clutter.Constraint,

    _init: function(thumbnailActor) {
        this.parent();

        this._thumbnailActor = thumbnailActor;
    },

    vfunc_update_allocation(actor, allocation) {
        let [, , w, h] = this._thumbnailActor.get_preferred_size();
        w *= this._thumbnailActor.scale_x;
        h *= this._thumbnailActor.scale_y;

        let node = actor.get_theme_node();
        let borderLeft = node.get_border_width(St.Side.LEFT);
        let borderTop = node.get_border_width(St.Side.TOP);
        let borderRight = node.get_border_width(St.Side.RIGHT);
        let borderBottom = node.get_border_width(St.Side.BOTTOM);

        allocation.set_origin(this._thumbnailActor.x - borderLeft,
                              this._thumbnailActor.y - borderTop);
        allocation.set_size(borderLeft + w + borderRight,
                            borderTop + h + borderTop);
    }
});

let dashSlider, thumbnailsBox;
let showAppsButton = null;

let addThumbnails = null;
let animateIn = null;
let fadePageOut = null;
let getShowAppsButton = null;
let toggleAppsPage = null;
let toggleOverview = null;

let pageChangedId = 0;
let showingId = 0;
let thumbnailsAllocateId = 0;

function init() {
    dashSlider = Main.overview._controls._dashSlider;
    thumbnailsBox = Main.overview._controls._thumbnailsBox;

    addThumbnails = WorkspaceThumbnail.ThumbnailsBox.prototype.addThumbnails;

    animateIn = ViewSelector.ViewSelector.prototype._animateIn;
    fadePageOut = ViewSelector.ViewSelector.prototype._fadePageOut;
    toggleAppsPage = ViewSelector.ViewSelector.prototype._toggleAppsPage;

    getShowAppsButton = Overview.Overview.prototype.getShowAppsButton;
    toggleOverview = Overview.Overview.prototype.toggle;
}

function enable() {
    dashSlider.slideIn = function() {};
    dashSlider.getVisibleWidth = function() { return 0; };
    dashSlider.slideOut();

    thumbnailsBox.addThumbnails = function(start, count) {
        addThumbnails.call(this, start, count);

        this._thumbnails.forEach(t => {
            if (t._border)
                return;

            t._border = new St.Widget({ style_class: 'thumbnail-border' });
            t._border.add_constraint(new ThumbBorderConstraint(t.actor));

            t.actor.connect('destroy', () => {
                t._border.destroy();
                delete t._border;
            });

            this.actor.insert_child_below(t._border, t.actor);
        });
    }

    thumbnailsAllocateId = thumbnailsBox.actor.connect('allocate',
        Lang.bind(thumbnailsBox, function() {
            this._thumbnails.forEach(t => {
                t._border.allocate_preferred_size(0);
            });
        }));

    AppsView.enable();

    showAppsButton = new AppsButton();
    Main.panel.addToStatusArea('show-apps', showAppsButton, 1, 'left');

    Main.overview.getShowAppsButton = () => showAppsButton.actor;

    Main.overview.viewSelector._animateIn = function(oldPage) {
        if (this._activePage == this._appsPage && oldPage == null)
            oldPage = this._workspacesPage;
        animateIn.call(this, oldPage);
    }

    Main.overview.viewSelector._fadePageOut = function(page) {
        let oldPage = page;
        if (page != this._appsPage ||
            this.getActivePage() == ViewSelector.ViewPage.SEARCH) {
            fadePageOut.call(this, page);
            return;
        }

        this.appDisplay.animate(IconGrid.AnimationDirection.OUT, () => {
            this._animateIn(oldPage);
        });
    }

    Main.overview.viewSelector._toggleAppsPage = function() {
        if (this._showAppsButton.checked)
            Main.overview.hide();
        else
            toggleAppsPage.call(this);
    }

    Main.overview.toggle = function() {
        if (this.visible &&
            this.viewSelector.getActivePage() == ViewSelector.ViewPage.APPS)
            this._dash.showAppsButton.checked = false;
        else
            toggleOverview.call(this);
    }

    pageChangedId = Main.overview.viewSelector.connect('page-changed',
        function(viewSelector) {
            let activePage = viewSelector.getActivePage();

            setSearchVisible(activePage != ViewSelector.ViewPage.WINDOWS);

            if (Main.overview.visible &&
                activePage == ViewSelector.ViewPage.SEARCH)
                return;

            let statusArea = Main.panel.statusArea;
            let activities = statusArea['activities'];
            let showApps = statusArea['show-apps'];

            [activities, showApps].forEach(b => {
                b.actor.remove_style_pseudo_class('overview');
            });

            if (!Main.overview.visible)
                return;

            if (activePage == ViewSelector.ViewPage.APPS)
                showApps.actor.add_style_pseudo_class('overview');
            else
                activities.actor.add_style_pseudo_class('overview');
        });

    showingId = Main.overview.connect('showing', function() {
        Main.overview.viewSelector.emit('page-changed');
    });

    reloadKeybindings();
    Main.loadTheme();
}

function disable() {
    let sliderProto = OverviewControls.SlidingControl.prototype;
    dashSlider.slideIn = sliderProto.slideIn;
    dashSlider.getVisibleWidth = sliderProto.getVisibleWidth;
    dashSlider._updateSlide();

    thumbnailsBox.addThumbnails = addThumbnails;
    thumbnailsBox.actor.disconnect(thumbnailsAllocateId);

    thumbnailsBox._thumbnails.forEach(t => {
        t._border.destroy();
        delete t._border;
    });

    AppsView.disable();

    showAppsButton.destroy();
    showAppsButton = null;

    Main.overview.getShowAppsButton = getShowAppsButton;
    Main.overview.toggle = toggleOverview;

    Main.overview.viewSelector._animateIn = animateIn;
    Main.overview.viewSelector._fadePageOut = fadePageOut;
    Main.overview.viewSelector._toggleAppsPage = toggleAppsPage;

    Main.overview.disconnect(showingId);

    Main.overview.viewSelector.disconnect(pageChangedId);

    setSearchVisible(true);

    reloadKeybindings();
    Main.loadTheme();
}
